from django.shortcuts import render, redirect
from todo.models import TodoList
from todo.forms import ListForm

# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {'todo_lists': todo_lists}
    return render(request, 'todo/list.html', context)

def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)

    context = {
        'todo_list': todo_list,
    }

    return render(request, 'todo/detail.html', context)

def todo_list_create(request):
    if request.method == 'POST':
        # We should use the form to validate the values
        # and save them to the database
        form = ListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            # form.save()
            # If all goes well, we can redirect the browser
            # to another page and leave the function
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        # create an instance of the Django model form class
        form = TodoList()
    # Put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "todo/create.html", context)

def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)

    if request.method == "POST":
        form = ListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = ListForm(instance=todo_list)

    context = {
        'form': form,
        'todo_list': todo_list,
    }

    return render(request, 'todo/edit.html', context)

def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)

    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    context = {
        'todo_list': todo_list,
    }

    return render(request, 'todo/delete.html', context)

def todo_item_create(request):
    if request.method == "POST":
        # Create an instance of the TodoItem model and populate it with form data
        form = ListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=form.cleaned_data['list'].id)

    else:
        form = ListForm()

    context = {
        'form': form,
    }

    return render(request, 'todo/item_create.html', context)
