from django.forms import ModelForm
from todo.models import TodoList

class ListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
